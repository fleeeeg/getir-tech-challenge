const BaseError = require('./base');

module.exports = class ValidationError extends BaseError {
	constructor(params) {
		super({
			message: 'Validation error',
			code: 400,
			...params
		});
	}
};
