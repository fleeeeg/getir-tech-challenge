const BaseError = require('./base');

module.exports = class NotFoundError extends BaseError {
	constructor(params) {
		super({
			message: 'URL is not found',
			code: 404,
			...params
		});
	}
};
