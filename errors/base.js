const _ = require('underscore');

module.exports = class BaseError extends Error {
	constructor({message = 'Unknown error', code = 500, ...params}) {
		super(message);

		this.code = code;

		_(this).extend(params);
	}
};
