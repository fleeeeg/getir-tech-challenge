const BaseError = require('./base');

module.exports = class ServerError extends BaseError {
	constructor(params) {
		super({
			message: 'Internal server error',
			code: 500,
			...params
		});
	}
};
