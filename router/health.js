const {createResponse, createRouter} = require('../utils/express');

const router = createRouter();

router.get('/', (req, res) => {
	res.json(createResponse());
});

module.exports = router;
