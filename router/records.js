const {
	asyncHandler,
	createResponse,
	createRouter
} = require('../utils/express');
const db = require('../db');
const {validate} = require('../utils/validator');
const RecordsService = require('../services/records');

const router = createRouter();
const service = new RecordsService({db});

const schema = {
	type: 'object',
	properties: {
		startDate: {
			type: 'string',
			format: 'date'
		},
		endDate: {
			type: 'string',
			format: 'date',
			formatExclusiveMinimum: {$data: '1/startDate'}
		},
		minCount: {
			type: 'integer',
			minimum: 0
		},
		maxCount: {
			type: 'integer',
			exclusiveMinimum: {$data: '1/minCount'}
		}
	},
	required: ['startDate', 'endDate', 'minCount', 'maxCount'],
	additionalProperties: false
};

router.post('/', asyncHandler(async (req, res) => {
	const params = validate(schema, req.body);
	const records = await service.find(params);

	res.json(createResponse({records}));
}));

module.exports = router;
