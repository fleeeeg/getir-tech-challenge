const {createRouter} = require('../utils/express');
const healthRouter = require('./health');
const recordsRouter = require('./records');

const router = createRouter();

router.use('/health', healthRouter);
router.use('/records', recordsRouter);

module.exports = router;
