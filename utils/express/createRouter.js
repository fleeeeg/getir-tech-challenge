const {Router} = require('express');

module.exports = (options) => new Router({
	caseSensitive: true,
	mergeParams: true,
	...options
});
