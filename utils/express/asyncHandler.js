module.exports = (handler) => async (req, res, next) => {
	Promise.resolve(handler(req, res)).catch(next);
};
