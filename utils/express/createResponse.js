module.exports = ({message = 'Success', code = 0, ...rest} = {}) => ({
	code,
	msg: message,
	...rest
});
