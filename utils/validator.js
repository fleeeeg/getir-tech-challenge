const Ajv = require('ajv');
const formats = require('ajv-formats');
const {ValidationError} = require('../errors');

const ajv = new Ajv({
	$data: true,
	coerceTypes: true,
	useDefaults: true
});

formats(ajv);

const formatErrors = () => ajv.errorsText(ajv.errors);

exports.validate = (schema, value) => {
	if (!ajv.validate(schema, value)) {
		throw new ValidationError({message: formatErrors(), schema, value});
	}

	return value;
};
