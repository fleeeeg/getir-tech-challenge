const pino = require('pino');
const config = require('../config');

module.exports = pino({
	name: 'main',
	level: config.logger.level
});
