require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const db = require('./db');
const logger = require('./utils/logger');
const p = require('./utils/promise');
const router = require('./router');
const notFound = require('./middlewares/notFound');
const loggerMiddleware = require('./middlewares/logger');
const errorHandler = require('./middlewares/errorHandler');

async function start() {
	const app = express();

	await db.init();

	app.disable('x-powered-by');
	app.set('env', config.env);
	app.set('config', config);
	app.set('db', db);
	app.set('case sensitive routing', true);

	if (config.env === 'development') {
		app.set('json spaces', 2);
	}

	app.use(loggerMiddleware());
	app.use(bodyParser.json());
	app.use(router);
	app.use(notFound());
	app.use(errorHandler());

	const server = app.listen(config.port, config.hostname);

	await p.event(server, 'listening');

	logger.info(`listening server on ${config.hostname}:${config.port}`);

	app.set('server', server);

	return app;
}

exports.start = start;

if (require.main === module) {
	start().catch((err) => {
		logger.fatal(err);
		process.exit(1);
	});
}
