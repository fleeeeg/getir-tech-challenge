const {MongoClient} = require('mongodb');
const config = require('./config');
const logger = require('./utils/logger').child({name: 'mongodb'});

let initialized = false;

exports.init = async () => {
	if (initialized) {
		return;
	}

	const client = await MongoClient(
		config.mongodb.url,
		{useUnifiedTopology: true}
	);

	let connected = false;
	let onceConnected = false;

	client.on('serverHeartbeatSucceeded', () => {
		onceConnected = true;

		if (!connected) {
			logger.info('connection has been established');
			connected = true;
		}
	});

	client.on('serverHeartbeatFailed', () => {
		if (connected) {
			connected = false;
			logger.warn('lost connection');
		}

		if (onceConnected) {
			logger.warn('try to reconnect');
		} else {
			logger.warn('try to connect');
		}
	});

	await client.connect();

	const db = client.db();

	exports.records = db.collection('records');

	exports.close = () => client.close();

	initialized = true;
};
