# getir-tech-challenge

## Requirements
- node.js 14 (tested on v14.15.1)
- mongodb 4.4 (tested on v4.4.6)

Additional development dependencies:

- docker 20.10
- docker-compose 1.27

## Installation
Run `npm i` to install dependencies.

Create `.env` file in the root of project, you can copy `.env.example` and change required values.

Without `.env` file default values are used, which are:
```
NODE_ENV=development
HOSTNAME=127.0.0.1
PORT=9000
MONGODB_URL=mongodb://127.0.0.1:27017/records
LOGGER_LEVEL=info
```

## Development
Run `npm run dev` to start and watch project.

## Test
Start local instance of mongodb via `docker-compose up -d`.

Run `npm run test`.

By default logs with level less than `fatal` are disabled in test env.
To see logs, change `test/.env` file or pass variable `LOGGER_LEVEL=info`

## API
Project expose one route `POST /records` to retrieve list of the records

### Parameters
| Name  | Format  | Required  | Description  |
|---|---|---|---|
| `startDate` | string `YYYY-MM-DD` | true | filter records where `createdAt` >= `startDate`|
| `endDate`   | string `YYYY-MM-DD` | true | filter records where `createdAt` < `endDate` |
| `minCount` | integer | true | filter records where `totalCount` >= `minCount` |
| `maxCount` | integer | true | filter records where `totalCount` < `maxCount` |

### Response
Response contains body with followed schema:
```json
{
	"type": "object",
	"properties": {
		"code": {
			"type": "integer",
			"enum": [0, 400, 404, 500]
		},
		"msg": {"type": "string", "minLength": 1},
		"records": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"key": {"type": "string", "minLength": 1},
					"createdAt": {"type": "string", "format": "date-time"},
					"totalCount": {"type": "integer", "minimum": 0}
				},
				"required": ["key", "createdAt", "totalCount"],
				"additionalProperties": false
			}
		}
	},
	"required": ["code", "msg"],
	"additionalProperties": false
}
```
In case of error `records` field is omitted.

### Codes
- `0` - success
- `400` - invalid request
- `404` - url not found
- `500` - internal server error

HTTP response codes are the same, except `200` in success case.

### Example
```sh
$ curl -X POST http://127.0.0.1:9000/records -H "Content-Type: application/json" -d '{"minCount":10,"maxCount":15000,"startDate":"2016-01-26","endDate":"2016-01-27"}'

{
  "code": 0,
  "msg": "Success",
  "records": [
    {
      "key": "NzlSeeEE",
      "createdAt": "2016-01-26T20:14:42.976Z",
      "totalCount": 140
    },
    {
      "key": "gAUlkgox",
      "createdAt": "2016-01-26T15:27:39.027Z",
      "totalCount": 100
    }
  ]
}
```