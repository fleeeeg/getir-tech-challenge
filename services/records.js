module.exports = class RecordsService {
	constructor({db}) {
		this.db = db;
	}

	async find({startDate, endDate, minCount, maxCount}) {
		const records = await this.db.records.aggregate([
			{
				$match: {
					createdAt: {
						$gte: new Date(startDate),
						$lt: new Date(endDate)
					}
				}
			},
			{
				$project: {
					_id: 0,
					key: 1,
					createdAt: 1,
					totalCount: {$sum: '$counts'}
				}
			},
			{
				$match: {
					totalCount: {$gte: minCount, $lt: maxCount}
				}
			},
			{$sort: {createdAt: -1}}
		]).toArray();

		return records;
	}
};
