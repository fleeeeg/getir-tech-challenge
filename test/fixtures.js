exports.payload = (params) => ({
	startDate: '2017-01-28',
	endDate: '2017-01-29',
	minCount: 1,
	maxCount: 100000,
	...params
});

exports.records = () => [
	{
		key: 'a',
		createdAt: new Date('2017-01-28T01:22:14.398Z'),
		counts: [150, 160]
	},
	{key: 'a', createdAt: new Date('2017-01-28T01:22:14.398Z'), counts: [170]},
	{key: 'a', createdAt: new Date('2017-01-28T01:22:14.398Z'), counts: [120]},
	{key: 'b', createdAt: new Date('2016-12-30T11:56:25.780Z'), counts: [77, 43]},
	{
		key: 'c',
		createdAt: new Date('2016-12-30T04:51:57.295Z'),
		counts: [1855, 1255, 1074]
	},
	{
		key: 'd',
		createdAt: new Date('2016-12-30T04:37:04.145Z'),
		counts: [308, 834, 875, 475, 438, 1046]
	},
	{
		key: 'd',
		createdAt: new Date('2016-12-30T04:37:04.145Z'),
		counts: [308, 834, 875, 475, 438, 1046]
	},
	{key: 'c', createdAt: new Date('2016-12-30T01:31:07.831Z'), counts: [58, 58]},
	{key: 'd', createdAt: new Date('2016-12-29T23:54:11.832Z'), counts: [92, 62]},
	{
		key: 'e',
		createdAt: new Date('2016-12-29T23:43:32.214Z'),
		counts: [811, 731, 350, 924, 943, 882]
	}
];
