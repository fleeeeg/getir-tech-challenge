const supertest = require('supertest');
const {start} = require('../app');
const p = require('../utils/promise');
const fixtures = require('./fixtures');

describe('records test', () => {
	let app;
	let server;
	let db;
	let request;
	let config;

	beforeAll(async () => {
		app = await start();
		server = app.get('server');
		db = app.get('db');
		config = app.get('config');
		request = supertest(`http://${config.hostname}:${config.port}`);
	});

	describe('errors', () => {
		it('not found', async () => {
			await request
				.post('/')
				.expect(404, {
					code: 404,
					msg: 'URL is not found'
				});
		});

		it('additional request properties', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({foo: 'bar'}))
				.expect(400, {
					code: 400,
					msg: 'data must NOT have additional properties'
				});
		});

		it('invalid start date', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({startDate: 'bar'}))
				.expect(400, {
					code: 400,
					msg: 'data/startDate must match format "date"'
				});
		});

		it('invalid end date', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({endDate: 'bar'}))
				.expect(400, {
					code: 400,
					msg: 'data/endDate must match format "date"'
				});
		});

		it('start date greater than end date', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({startDate: '2021-06-05', endDate: '2021-06-01'}))
				.expect(400, {
					code: 400,
					msg: 'data/endDate should be > 2021-06-05'
				});
		});

		it('start date equal end date', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({startDate: '2021-06-05', endDate: '2021-06-05'}))
				.expect(400, {
					code: 400,
					msg: 'data/endDate should be > 2021-06-05'
				});
		});

		it('invalid min count', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({minCount: 'bar'}))
				.expect(400, {
					code: 400,
					msg: 'data/minCount must be integer'
				});
		});

		it('invalid max count', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({maxCount: 'bar'}))
				.expect(400, {
					code: 400,
					msg: 'data/maxCount must be integer'
				});
		});

		it('min count greater than max count', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({minCount: 10, maxCount: 1}))
				.expect(400, {
					code: 400,
					msg: 'data/maxCount must be > 10'
				});
		});

		it('min count equal max count', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({minCount: 10, maxCount: 10}))
				.expect(400, {
					code: 400,
					msg: 'data/maxCount must be > 10'
				});
		});
	});

	describe('find', () => {
		beforeAll(async () => {
			await db.records.insertMany(fixtures.records());
		});

		it('empty', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({startDate: '2021-06-01', endDate: '2021-06-05'}))
				.expect(200, {
					code: 0,
					msg: 'Success',
					records: []
				});
		});

		it('by date', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({startDate: '2016-12-29', endDate: '2016-12-30'}))
				.expect(200, {
					code: 0,
					msg: 'Success',
					records: [
						{key: 'd', createdAt: '2016-12-29T23:54:11.832Z', totalCount: 154},
						{key: 'e', createdAt: '2016-12-29T23:43:32.214Z', totalCount: 4641}
					]
				});
		});

		it('by counts', async () => {
			await request
				.post('/records')
				.send(fixtures.payload({
					startDate: '2016-12-29',
					endDate: '2020-12-30',
					minCount: 200,
					maxCount: 4000
				}))
				.expect(200, {
					code: 0,
					msg: 'Success',
					records: [
						{key: 'a', createdAt: '2017-01-28T01:22:14.398Z', totalCount: 310},
						{key: 'd', createdAt: '2016-12-30T04:37:04.145Z', totalCount: 3976},
						{key: 'd', createdAt: '2016-12-30T04:37:04.145Z', totalCount: 3976}
					]
				});
		});

		afterAll(async () => {
			await db.records.deleteMany({});
		});
	});

	afterAll(async () => {
		server.close();
		await p.event(server, 'close');
		await db.close();
	});
});
