const pino = require('express-pino-logger');
const logger = require('../utils/logger');

module.exports = () => pino({logger});
