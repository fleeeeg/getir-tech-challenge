const config = require('../config');
const {BaseError, ServerError} = require('../errors');
const logger = require('../utils/logger');
const {createResponse} = require('../utils/express');

// 4 arguments are required for error handling middleware
// eslint-disable-next-line no-unused-vars
module.exports = () => (err, req, res, next) => {
	const isExposable = err instanceof BaseError;
	const code = err.code || 500;

	logger.error(err);

	if (!isExposable && config.env === 'production') {
		err = new ServerError();
	}

	res.status(code);

	res.json(createResponse({
		code,
		message: err.message
	}));
};
