const {NotFoundError} = require('../errors');

module.exports = () => () => {
	throw new NotFoundError();
};
