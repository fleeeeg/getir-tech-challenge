const _ = require('underscore');
const {validate} = require('./utils/validator');

const get = (name, modifier, fallback) =>
	_.isUndefined(process.env[name]) ? fallback : modifier(process.env[name]);

const config = {
	env: get('NODE_ENV', String, 'development'),

	hostname: get('HOSTNAME', String, '127.0.0.1'),
	port: get('PORT', Number, 9000),

	mongodb: {
		url: get('MONGODB_URL', String, 'mongodb://127.0.0.1:27017/records')
	},

	logger: {
		level: get('LOGGER_LEVEL', String, 'info')
	}
};

const schema = {
	type: 'object',
	properties: {
		env: {
			type: 'string',
			enum: ['development', 'production', 'test']
		},
		hostname: {
			type: 'string',
			format: 'hostname'
		},
		port: {
			type: 'integer',
			minimum: 1,
			maximum: 65535
		},
		mongodb: {
			type: 'object',
			properties: {
				url: {
					type: 'string',
					format: 'uri'
				}
			},
			required: ['url'],
			additionalProperties: false
		},
		logger: {
			type: 'object',
			properties: {
				level: {
					type: 'string',
					enum: ['trace', 'debug', 'info', 'warn', 'error', 'fatal', 'silent']
				}
			},
			required: ['level'],
			additionalProperties: false
		}
	},
	required: ['env', 'hostname', 'port', 'mongodb', 'logger'],
	additionalProperties: false
};


validate(schema, config);

module.exports = config;
